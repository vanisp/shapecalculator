package com.company.xxx;

import com.company.xxx.shapes.Circle;
import com.company.xxx.shapes.Rectangle;
import com.company.xxx.shapes.Square;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProductionDatabase implements Database {

    private List<Object> SHAPES = Arrays.asList(
            new Rectangle(12d, 126666d),
            new Circle(123d),
            new Square(500d),
            new Rectangle(12d, 12d),
            new Circle(127d),
            new Square(1000d),
            new Rectangle(12.8d, 12d),
            new Circle(123d),
            new Square(500d),
            new Rectangle(12d, 127d),
            new Circle(123d),
            new Square(800d),
            new Rectangle(12d, 12d),
            new Circle(1234d),
            new Square(500d),
            new Rectangle(12d, 12d),
            new Circle(123d),
            new Square(500d)
    );

    @Override
    public List findShapes() {
        return Collections.unmodifiableList(SHAPES);
    }
}
