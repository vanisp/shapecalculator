package com.company.xxx.test;

import com.company.xxx.Database;

import java.util.Collections;
import java.util.List;

public class EmptyDatabaseForTesting implements Database {

    @Override
    public List findShapes() {
        return Collections.emptyList();
    }
}
