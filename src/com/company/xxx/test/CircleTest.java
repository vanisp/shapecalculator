package com.company.xxx.test;

import com.company.xxx.Application;
import com.company.xxx.Database;
import com.company.xxx.Main;
import com.company.xxx.shapes.Circle;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CircleTest {

    @Before
    public void setUp() throws Exception {
        Application a = new Application(new Main(), new EmptyDatabaseForTesting());
    }

    @Test
    public void calculateCircumference() {
        Circle c = new Circle(10d);
        assertEquals(new Double(62.83185307179586), c.calculateCircumference());

    }

    @Test
    public void calculateArea() {
        Circle c = new Circle(10d);
        assertEquals(new Double(314.1592653589793), c.calculateArea());
    }
}