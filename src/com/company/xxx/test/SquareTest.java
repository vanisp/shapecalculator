package com.company.xxx.test;

import com.company.xxx.Application;
import com.company.xxx.Main;
import com.company.xxx.shapes.Square;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTest {

    @Before
    public void setUp() throws Exception {
        Application a = new Application(new Main(), new EmptyDatabaseForTesting());
    }

    @Test
    public void calculateCircumference() {
        Square s = new Square(10d);
        assertEquals(new Double(40), s.calculateCircumference());
    }

    @Test
    public void calculateArea() {
        Square s = new Square(10d);
        assertEquals(new Double(100), s.calculateArea());
    }
}