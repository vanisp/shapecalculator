package com.company.xxx.test;

import com.company.xxx.Application;
import com.company.xxx.Database;
import com.company.xxx.Main;
import com.company.xxx.shapes.Circle;
import com.company.xxx.shapes.Rectangle;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void run() {
        Application app = new Application(new Main(), new Database() {
            @Override
            public List findShapes() {
                return Arrays.asList(
                        new Rectangle(1d, 2d),
                        new Circle(1d)
                );
            }
        });

        app.run();

        assertEquals(new Double(5.141592653589793), app.getSumOfAreas());
        assertEquals(new Double(12.283185307179586), app.getSumOfCircumferences());
    }
}