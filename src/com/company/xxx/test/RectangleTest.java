package com.company.xxx.test;

import com.company.xxx.Application;
import com.company.xxx.Main;
import com.company.xxx.shapes.Rectangle;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    @Before
    public void setUp() throws Exception {
        Application a = new Application(new Main(), new EmptyDatabaseForTesting());
    }

    @Test
    public void calculateCircumference() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(new Double(12), rectangle.calculateCircumference());
        rectangle.setWidth(2d);
        assertEquals(new Double(14), rectangle.calculateCircumference());
    }

    @Test
    public void calculateArea() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(new Double(5.0), rectangle.calculateArea());
        rectangle.setWidth(2d);
        assertEquals(new Double(10), rectangle.calculateArea());
    }

    @Test
    public void getWidth() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(new Double(1), rectangle.getWidth());
    }

    @Test
    public void getHeight() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(new Double(5), rectangle.getHeight());
    }

    @Test
    public void setWidth() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        rectangle.setWidth(2d);
        assertEquals(new Double(2), rectangle.getWidth());
        assertEquals(1, Application.getMain().getCOUNTER());
    }

    @Test
    public void setHeight() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        rectangle.setHeight(2d);
        assertEquals(new Double(2), rectangle.getHeight());
    }
}