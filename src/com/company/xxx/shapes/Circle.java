package com.company.xxx.shapes;

public class Circle {

    private Double r;

    public Circle(Double r) {
        this.r = r;
    }

    public Double calculateCircumference() {
        return 2 * Math.PI * r;
    }

    public Double calculateArea() {
        return Math.PI * r * r;
    }
}
