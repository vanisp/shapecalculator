package com.company.xxx.shapes;

import com.company.xxx.Application;

public class Rectangle {

    private Double width;
    private Double height;

    public Rectangle(Double width, Double height) {
        this.width = width;
        this.height = height;
    }

    public Double calculateCircumference (){
        return 2 * width + 2 * height;
    }

    public Double calculateArea () {
        return width * height;
    }

    public Double getWidth() {
        return width;
    }

    public Double getHeight() {
        return height;
    }

    public void setWidth(Double width) {
        // we do have a business rule to count and report how many times the width changes
        Application.getMain().incrementCounter();
        this.width = width;
    }

    public void setHeight(Double height) {
        this.height = height;
    }
}
