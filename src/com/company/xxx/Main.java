package com.company.xxx;

public class Main {

    private int COUNTER = 0;

    public static void main(String[] args) {
	    Application app = new Application(new Main(), new ProductionDatabase());
	    app.run();

        System.out.println("Sum of areas: " + app.getSumOfAreas());
        System.out.println("Sum of circumferences " + app.getSumOfCircumferences());
    }

    public void incrementCounter(){
        COUNTER++;
    }

    public void reportChanges () {
        System.out.println(COUNTER + " changes in width occured");
    }

    // to be used only by tests
    public int getCOUNTER() {
        return COUNTER;
    }
}
