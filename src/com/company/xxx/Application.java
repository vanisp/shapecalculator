package com.company.xxx;

import com.company.xxx.shapes.Circle;
import com.company.xxx.shapes.Rectangle;
import com.company.xxx.shapes.Square;

import java.util.List;

public class Application {


    private static Main main;

    private Database database;

    private Double sumOfAreas = 0d;
    private Double sumOfCircumferences = 0d;

    public Application(Main main, Database database) {
        Application.main = main;
        this.database = database;
    }

    public void run() {
        for(Object o : loadShapesFromDatabase()){
            if (o instanceof Rectangle){
                Rectangle r = (Rectangle) o;
                // We know that in the database we have some corrupted data
                // so each rectangles width and height must be swapped before calculation
                // just to be sure
                Double width = r.getWidth();
                Double height = r.getHeight();
                r.setWidth(height);
                r.setHeight(width);
                sumOfAreas += r.calculateArea();
                sumOfCircumferences += r.calculateCircumference();
            } else if (o instanceof Square) {
                Square s = (Square) o;
                Double area = s.calculateArea();
                // We have a business rule stating that square areas should count as double the actual area
                this.sumOfAreas += area * 2;
                sumOfCircumferences += s.calculateCircumference();
            } else if (o instanceof Circle){
                Circle c = (Circle) o;
                sumOfAreas += c.calculateArea();
                sumOfCircumferences += c.calculateCircumference();
            }
        }
        main.reportChanges();
    }

    public static Main getMain() {
        return main;
    }

    public Double getSumOfAreas() {
        return sumOfAreas;
    }

    public Double getSumOfCircumferences() {
        return sumOfCircumferences;
    }

    private List loadShapesFromDatabase(){
        return database.findShapes();
    }
}
